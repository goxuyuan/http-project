
class Header(object):

	data = ''
	
	@property
	def method(self):
		return self.data.split(' ')[0]	
		

	@property
	def path_info(self):
		path = self.data.split(' ')[1]
		if '?' in path:
			return path.split('?')[0]
		return self.data.split(' ')[1]

	@property
	def paramter(self):
		if self.method == 'GET':
			return self.getParaSrc(self.data)
		elif self.method == 'POST':
			item = self.data.split('\r\n')[-1]
			arr = item.split('&')
			params = dict()
			for key_value in arr:
				items = key_value.split('=')
				length = len(items)
				if length == 0:
					continue
				params[items[0]] = items[-1]
			return params
		else:
			return self.data

	def __init__(self,data):
		self.data = data
	
	def getParaSrc(self,data):

	    src =  self.path_info
	    if not ('?' in src):
	        return
	    values = src.split('?')[-1]
	    params = dict()
	    for key_value in values.split('&'):
	        items = key_value.split('=')
	        params[items[0]] = items[-1]
	    return params