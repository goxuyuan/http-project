# -*- coding: utf-8 -*-

import socket
import select
import Queue
# import HeaderUtils

from consts import STATUS_CODE, DEFAULT_ERROR_MESSAGE

from header import Header


class RequestHandler(object):
    rbufsize = -1
    wbufsize = 0
    timeout = None
    max_bytes = 2 ** 16
    protocol_version = 'HTTP/1.1'
    content = ''
    inputs = []
    outputs = []

    msg_dic = {}

    def __init__(self, server):
        # sockets from which we except to read
        inputs = [server.socket]

        # sockets from which we expect to write
        outputs = []

        # Outgoing message queues (socket:Queue)
        message_queues = {}

        # A optional parameter for select is TIMEOUT
        # timeout = 20

        while inputs:

            print "waiting for next event"
            readable, writable, exceptional = select.select(inputs, outputs, inputs)
            print inputs
            print outputs
            print message_queues

            # When timeout reached , select return three empty lists
            # if not (readable or writable or exceptional):
            #     print "Time out ! "
            #     break;
            for s in readable:
                if s is server.socket:
                    # A "readable" socket is ready to accept a connection
                    connection, client_address = s.accept()
                    print "    connection from ", client_address
                    connection.setblocking(0)
                    inputs.append(connection)
                    message_queues[connection] = Queue.Queue()
                else:
                    data = s.recv(1024)


                    if data:
                        print " received ", data, "from ", s.getpeername()
                        message_queues[s].put(data)
                        # Add output channel for response
                        if s not in outputs:
                            outputs.append(s)
                    else:
                        # Interpret empty result as closed connection
                        print "  closing", client_address
                        if s in outputs:
                            outputs.remove(s)
                        inputs.remove(s)
                        s.close()
                        # remove message queue
                        del message_queues[s]
            for s in writable:

                try:
                    next_msg = message_queues[s].get_nowait()
                except Queue.Empty:
                    print " ", s.getpeername(), 'queue empty'
                    outputs.remove(s)
                else:
                    print " sending ", next_msg, " to ", s.getpeername()
                    self.rfile = next_msg
                    self.header = Header(next_msg)
                    self.handle_one_request()

                    s.send(self.content)





            # for s in exceptional:
            #     print " exception condition on ", s.getpeername()
            #     # stop listening for input on the connection
            #     inputs.remove(s)
            #     if s in outputs:
            #         outputs.remove(s)
            #     s.close()
            #     # Remove message queue
            #     del message_queues[s]

    def handle_one_request(self):

        if not self.rfile:
            self.requestline = ''
            self.command = ''
            # warning error  处理
            # self.send_error(414)
            return
        else:
            if not self.parse_request():
                return
            action = self.command
            print 'http 请求方法 。。。。。。。。', action
            method = getattr(self, action)

            path = self.filert_path(self.header.path_info)

            if not path:
                return
            else:
                method(path)

    def filert_path(self, path):
        if len(path) > 1:
            path = path[1:len(path)]
            if path == 'favicon.ico':
                return
        elif len(path) == 1 and path == '/':
            path = 'index.html'
        return path

    def parse_request(self):
        self.command = self.header.method.lower()
        return True

    def get(self, path):
        content = ''
        content += self.start_response(200)
        content += self.send_header('Content-Type', 'text/html')
        content += self.send_header('Connection', 'close')
        content += self.end_headers()

        file = open(path, 'rb')
        content += file.read()

        file.close()

        self.content = content

    # print self.content

    def post(self, path):
        print self.header.paramter
        post_param = str(self.header.paramter)

        content = ''
        content += self.start_response(200)
        content += self.send_header('Content-Type', 'text/html')
        content += self.end_headers()
        content += post_param
        content += '<br /><font color="green" size="7">register successs!</p>'
        self.content = content
        pass

    def start_response(self, code, message=None):
        if message is None:
            if code in STATUS_CODE:
                message = STATUS_CODE[code][0]
            else:
                message = ''
        return "%s %d %s\r\n" % (self.protocol_version, code, message)

    def send_header(self, keyword, value):
        return '%s:%s\r\n' % (keyword, value)

    def end_headers(self):
        return '\r\n'

    def finish(self):
        if len(self.content) == 0:
            return
        else:
            try:
                self.request.sendall(self.content)
            finally:
                self.content = ''
                self.request.close()

# self.setup()
# 		try:
# 			self.handle()
# 		finally:
# 			self.finish()

# 	def setup(self):
# 		self.connection = self.request
# 		# print 'sdfdsfsdfdsf', self.connection.recv(1024)

# 		# self.rfile = self.connection.recv(1024)
# 		# print 'sjdfljdfsl', self.rfile

# 		self.rfile = self.connection.makefile('rb',self.rbufsize)
# 		self.wfile = self.connection.makefile('wb',self.wbufsize)

# 	def handle(self):
# 		self.handle_one_request()
# 	def handle_one_request(self):
# 		print '....8*********'
# 		self.raw_requestline = self.rfile.read(self.max_bytes + 1)
# 		print '-----99999999---',self.raw_requestline


# 		if len(self.raw_requestline) > self.max_bytes:
# 			self.requestline = ''
# 			self.command = ''
# 			self.send_error(414)
# 			return
# 		if not self.parse_request():
# 			return
# 		action = 'do_' + self.command
# 		method = getattr(self,action)
# 		method()
# 		self.wfile.flush()

# 	def parse_request(self):

# 		self.command = None
# 		raw_requestline = self.raw_requestline
# 		requestline = raw_requestline.rstrip('\r\n')
# 		self.requestline = requestline
# 		words = requestline.split()
# 		if len(words) == 3:
# 			command, path, version = words
# 			base_version_number = version.split('/',1)[1]
# 			version_number = base_version_number.split('.')
# 		elif len(words) == 2:
# 			command, path = words
# 			version = 'HTTP/1.1'
# 		elif not words:
# 			return False
# 		else:
# 			self.send_error(404)
# 		self.command, self.path, self.request_version = command, path, version_number
# 		return True

# 	def finish(self):
# 		if not self.wfile.closed:
# 			try:
# 				self.wfile.flush()
# 			except socket.error:
# 				pass
# 		self.wfile.close()
# 		self.rfile.close()

# 	def send_error(self, code, message = None):
# 		try:
# 			shortm, longm = STATUS_CODE[code]

# 		except KeyError:
# 			shortm, longm = '???', '???'

# 		if  message is None:
# 			message = shortm
# 		explain = longm
# 		content = (self.error_message_format %
#             {'code': code, 'message': message, 'explain': explain})

# 		self.start_response(code, message)
# 		self.send_header('Content-Type',s)
# 		self.send_header('Connection', 'close')
# 		self.end_headers()
# 		if self.command != 'HEAD' and code >=200 and code not in (204, 304):
# 			self.wfile.write(content)

# 	error_message_format = DEFAULT_ERROR_MESSAGE


# 	def start_response(self, code, message = None):

# 		if message is None:
# 			if code in STATUS_CODE:
# 				message = STATUS_CODE[code][0]
# 			else:
# 				message = ''
# 		self.wfile.write("%s %d %s\r\n" % (self.protocol_version, code, message))

# 	def send_header(self,keyword,value):
# 		self.wfile.write('%s:%s\r\n'%(keyword,value))
# 	def end_headers(self):
# 		self.wfile.write('\r\n')


# class HttpRequestHandler(RequestHandler):
# 	def do_GET(self):
# 		self.hello_world()

# 	def hello_world(self):
# 		content = 'hello china'
# 		self.start_response(200)
# 		self.send_header('Content-type', 'text/plain')
# 		self.end_headers()
# 		self.wfile.write(content)
