#!/usr/bin/env python
#-*- coding: utf-8 -*-

# 运行服务端
# python file.py 
# get connection from : ('127.0.0.1', 56051) 
# 运行客户端
# python clientfile.py 
# >>put /Users/admin/Documents/TestPro/Python/http-project/test.jpg
# >>get /Users/admin/Documents/TestPro/Python/http-project/test.jpg


import SocketServer  
import time 
import os
#定义当前目录
current_dir = os.getcwd()

#定义一个类
class EddyFtpserver(SocketServer.BaseRequestHandler): 
	#定义接收文件方法
    def recvfile(self, filename): 
        print "starting reve file!"
        f = open(filename, 'wb') 
        self.request.send('ready') 
        while True: 
            data = self.request.recv(4096) 
            if data == 'EOF': 
                print "recv file success!"
                break
            f.write(data) 
        f.close() 
    #定义放送文件方法                                     
    def sendfile(self, filename): 
        print "starting send file!"
        self.request.send('ready') 
        time.sleep(1) 
        f = open(filename, 'rb') 
        while True: 
            data = f.read(4096) 
            if not data: 
                break
            self.request.sendall(data) 
        f.close() 
        time.sleep(1) 
        self.request.send('EOF') 
        print "send file success!"

    #SocketServer的一个方法                           
    def handle(self): 
        print "get connection from :",self.client_address 
        while True: 
            try: 
                data = self.request.recv(4096) 
                print "get data:", data    
                if not data: 
                    print "break the connection!"
                    break                
                else: 
                    action, filename = data.split() 
                    #判断上传
                    if action == "put": 
                        #上传文件保存到当前目录下
                        filename = current_dir + '/' + os.path.split(filename)[1]
                        self.recvfile(filename) 
                    #判断下载
                    elif action == 'get': 
                        self.sendfile(filename)  
                    else: 
                        print "get error!"
                        continue
            except Exception,e: 
                print "get error at:",e 


if __name__ == "__main__": 
    host = 'localhost' 
    port = 8888
    #实例化
    s = SocketServer.ThreadingTCPServer((host,port), EddyFtpserver) 
    s.serve_forever() 
