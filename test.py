# -*- coding: utf-8 -*-  
import socket
  
HOST = "127.0.0.1"  # The remote host
PORT = 9999  # The same port as used by the server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

s.close()
