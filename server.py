# -*- coding: utf-8 -*-

import socket
import threading
import time
import select
from handler import RequestHandler


class HttpServer(object):
    host = '127.0.0.1'

    def __init__(self, port, HandlerClass=RequestHandler):
        self.port = port or 8000
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_address = (self.host, self.port)

        self.HandlerClass = HandlerClass
        try:
            self.server_bind()
            self.server_activate()
            # self.socket.setblocking(0)
        except:
            self.server_close()
            raise

    def server_bind(self):
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(self.server_address)
        # self.socket.setblocking(False)   # 不阻塞
        self.server_address = self.socket.getsockname()

    def server_activate(self):
        self.socket.listen(5)

    def server_close(self):
        self.socket.close()

    def get_request(self):
        return self.socket.accept()

    def process_request(self, request, client_address):
        return self.HandlerClass(request, client_address, self)

    def server_forever(self):
        print 'server start ...........'

        self.HandlerClass(self)

    # HttpServer.tcplink(self)
    # self.process_request(request,client_address)
    # request,client_address = self.get_request()
    # t = threading.Thread(target=HttpServer.tcplink, args=(self,request, client_address))
    # t.start()

    def tcplink(self, request, client_address):
        print 'connection client-------------', client_address
        self.process_request(request, client_address)


httpd = HttpServer(9999)
httpd.server_forever()
